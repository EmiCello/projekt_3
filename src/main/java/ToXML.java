import com.test.xmlsorter.data.Printer;
import com.test.xmlsorter.data.PrinterList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class ToXML {

    public void toFile(PrinterList pl, String out) throws JAXBException {
        JAXBContext jxc = JAXBContext.newInstance(Printer.class, PrinterList.class);
        Marshaller jxm = jxc.createMarshaller();
        jxm.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jxm.marshal(pl, System.out);
        jxm.marshal(pl, new File(out));
    }
}
