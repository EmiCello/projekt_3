import com.test.xmlsorter.Komperator;
import com.test.xmlsorter.data.Printer;
import com.test.xmlsorter.data.PrinterList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class Demo {

    public static void main(String[] args) throws MalformedURLException, JAXBException {

        HttpDownloader httpDownloader = new HttpDownloader();
        File file = httpDownloader.download(new URL("https://princity.cloud/downloads/supported.xml"),
                new File("C:\\Users\\Emilia\\Desktop\\downloaded"));

        JAXBContext jaxbContext = JAXBContext.newInstance(Printer.class, PrinterList.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        PrinterList pl = (PrinterList) jaxbUnmarshaller.unmarshal(file);
        pl.getPrinterList().sort(new Komperator());
        System.out.println(pl.getPrinterList());

        ToXML toXML = new ToXML();
        toXML.toFile(pl,"Zapis.xml");

    }
}
